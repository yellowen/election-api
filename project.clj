(defproject election-api "0.1.0-SNAPSHOT"
  :description "Election API software"
  :url "http://election.ir"
  :license {:name "MIT"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.immutant/web "2.1.1"
                  :exclusions [ch.qos.logback/logback-classic]]
                 [com.taoensso/timbre "4.1.4"]
                 [com.fzakaria/slf4j-timbre "0.2.1"]
                 [compojure "1.4.0"]
                 [clj-json "0.5.3"]
                 [korma "0.4.0"]
                 [org.postgresql/postgresql "9.2-1002-jdbc4"]
                 [ring/ring-core "1.4.0"]
                 [environ "1.0.1"]]
  :plugins [[lein-environ "1.0.1"]]
  :main election-api.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:env {:api-host    "0.0.0.0"
                         :api-port    "4000"
                         :pg-db       "ellection"
                         :pg-user     "ellection_db_user"
                         :pg-password "ellection_DB_!"
                         :pg-host     "localhost"}}
             :text {:env {:api-host "0.0.0.0"
                          :api-port "4000"}}
             :production {:env {:api-host "0.0.0.0"
                                :api-port "80"}}})

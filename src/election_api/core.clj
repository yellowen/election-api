(ns election-api.core
  (:require
   [immutant.util            :as util]
   [taoensso.timbre          :as timbre]
   [election-api.websocket   :as ws])
  (:gen-class))

(defn start [args]
  (when util/dev-mode?
    (timbre/set-level! :debug))

  (ws/start args))

(defn stop [& {:as args}]
  (ws/stop))

(defn -main [& {:as args}]
  (start args)
  )

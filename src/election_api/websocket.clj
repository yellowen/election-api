(ns election-api.websocket
  (:require
   [immutant.web             :as web]
   [immutant.web.async       :as async]
   [immutant.web.middleware  :as web-middleware]
   [compojure.route          :as route]
   [clj-json.core            :as json]
   [taoensso.timbre          :as timbre]
   [environ.core             :refer (env)]
   [compojure.core           :refer (ANY GET defroutes)]
   [taoensso.timbre          :as timbre]
   [ring.util.response       :refer (response redirect content-type)]))


(defn call-command [ch command packet]
  (let [func-name (str command "-command")
        func (ns-resolve 'election-api.api.commands (symbol func-name))]
    (println func-name)
    (println func)
    (if func
      (func ch packet)
      (async/send! ch "Command did not implemented yet."))))


(def websocket-callbacks
  "WebSocket callback functions"
  {:on-open   (fn [channel]
                (println "New connect received"))

   :on-close   (fn [channel {:keys [code reason]}]
                 (println "close code:" code "reason:" reason))
   :on-message (fn [ch m]
                 (let [packet (json/parse-string m)
                       command (clojure.string/trim (get packet "command" "wrong"))]
                   (println (count command))
                   (call-command ch command packet)))})


(defroutes routes
  (GET "/" {c :context} (redirect (str c "/index.html")))
  (route/resources "/"))

(defn start [args]
  (timbre/info (str "Running On " (env :api-host) ":" (env :api-port)))
  (web/run (-> routes
               (web-middleware/wrap-session {:timeout 30})
               ;; wrap the handler with websocket support
               ;; websocket requests will go to the callbacks, ring requests to the handler
               (web-middleware/wrap-websocket websocket-callbacks))
    (merge {"host" (env :api-host), "port" (env :api-port)}
           args)))

(defn stop [& args]
  (web/stop))

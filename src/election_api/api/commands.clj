(ns election-api.api.commands
  (:require
   [election-api.db          :as db]
   [immutant.web.async       :as async]
   [clj-json.core            :as json]))

(defn areas-list-command [ch packet province-id]
  (let [areas (db/areas-for-province province-id)]
    (async/send! ch (json/generate {:command "area-list" :result areas}))))

(defn candidates-list-command [ch packet area]

  (async/send! ch (str "candidates for " area)))

(defn wrong-command
  "Issue on a wrong command"
  [ch packet]
  (async/send! ch (str "Wrong command '" ("command" packet) "'")))

(ns election-api.db
  (:require
   [korma.db       :as k]
   [korma.core     :refer :all :exclude [update]]
   [immutant.util  :as util]
   [environ.core   :refer (env)]))


(defn env-db
  "Return the appropriate database settings."
  []
  (k/postgres {:db        (env :pg-db)
               :user      (env :pg-user)
               :host      (env :pg-host)
               :password  (env :pg-password)}))

(k/defdb db (env-db))

(declare candidate announcement area comments event faq province)

(defentity province
  (table :provinces)
  (entity-fields :name)
  (has-many area {:fk "province_id"}))

(defentity area
  (table :areas)
  (entity-fields :name)
  (belongs-to province {:fk "province_id"}))

(defentity candidate
  (table :candidates)
  (entity-fields :name :family :slogan)
  (belongs-to area {:fk "area_id"}))

(defn areas-for-province [id]
  (select area
          (fields :id)
          (join :inner province
                (= :provinces.id :areas.province_id))
          (where {:province_id id})))

(defn candidates-for-area [id]
    (select candidate
            (fields :id)
            (join :inner area
                  (= :areas.id :candidates.area_id))
            (where {:area_id id})))
